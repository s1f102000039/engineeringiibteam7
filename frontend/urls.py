from django.urls import path
from .views import index

urlpatterns = [
    path('', index),
    path('word', index),
    path('signup', index),
    path('login', index),
    path('logout', index),
    path('forgot-password', index),
]