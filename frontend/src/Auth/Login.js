import React, {useRef, useState} from 'react'
import {Form, Card, Alert} from 'react-bootstrap'
import {Link, useHistory} from 'react-router-dom'
import {Container} from '@material-ui/core'
import {getCookie} from '../helper'
import {Button, Grid} from '@material-ui/core'
import {CSSTransition} from 'react-transition-group'
import {Modal} from 'react-bootstrap'
import Slide from 'react-reveal/Slide'
import LoginIcon from '@mui/icons-material/Login'
export default function Login(props) {
  const emailRef = useRef()
  const passwordRef = useRef()
  const history = useHistory()
  function onSignInButton() {
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
      body: JSON.stringify({
        username: emailRef.current.value,
        password: passwordRef.current.value,
      }),
    }
    if (!emailRef.current.value || !passwordRef.current.value) {
      props.Notification({
        title: 'Error',
        message: 'Username or password is empty!',
        type: 'danger',
        container: 'bottom-left',
      })
    } else {
      fetch('/rest-auth/login/', requestOptions)
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          if (data['non_field_errors']) {
            // Notification({type: 'danger', message: error})
            props.Notification({
              title: 'Error',
              message: data['non_field_errors'][0],
              type: 'danger',
              container: 'bottom-left',
            })
          } else {
            console.log(data.key)
            props.onLogin(data.key)
            props.Notification({
              title: 'Success',
              message: 'Login Successfully',
              type: 'success',
              container: 'bottom-left',
            })

            history.push('/')
            history.go(0)
          }
        })
        .catch((error) => {
          props.Notification({
            title: 'Error',
            message: error,
            type: 'danger',
            container: 'bottom-left',
          })
          console.log(error)
        })
    }
  }

  function onKeyPress(e) {
    if (e.key === 'Enter') {
      onSignInButton()
    }
  }
  return (
    <div style={{margin: '5px'}}>
      <CSSTransition in={props.loginAction} timeout={1000} classNames="menu">
        {props.loginAction !== false ? (
          <Card style={{backgroundColor: 'white'}}>
            <Card.Body>
              <Form>
                <Form.Group id="email">
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    type="text"
                    ref={emailRef}
                    style={{backgroundColor: '#F2F2F2'}}
                    required
                    onKeyDown={(e) => onKeyPress(e)}
                  />
                </Form.Group>
                <Form.Group id="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    ref={passwordRef}
                    required
                    style={{backgroundColor: '#F2F2F2'}}
                    autoComplete="on"
                    onKeyDown={(e) => onKeyPress(e)}
                  />
                </Form.Group>
                <Button
                  variant="outlined"
                  style={{backgroundColor: '#2C2E3A', color: 'white', marginTop: '15px'}}
                  onClick={onSignInButton}
                >
                  <LoginIcon /> Login
                </Button>
              </Form>
            </Card.Body>
          </Card>
        ) : (
          <div></div>
        )}
      </CSSTransition>
    </div>
  )
}
