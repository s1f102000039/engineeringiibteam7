import {Container} from '@material-ui/core'
import {width} from '@mui/system'
import React, {useEffect, useRef, useState} from 'react'
import {Form, Card} from 'react-bootstrap'
import {useHistory} from 'react-router-dom'
import {getCookie} from '../helper'
import Alert from '@material-ui/lab/Alert'
import {Link} from 'react-router-dom'
import {Button} from '@material-ui/core'
import {CSSTransition} from 'react-transition-group'
import {Modal} from 'react-bootstrap'
export default function Signup(props) {
  const usernameRef = useRef()
  const passwordRef = useRef()
  const passwordConfirmRef = useRef()
  const history = useHistory()
  // const [transitState, settransitState] = useState(false)
  async function onSignUpButton() {
    if (passwordRef.current.value != passwordConfirmRef.current.value) {
      props.Notification({
        message: 'Passwords do not match',
        title: 'Error',
        type: 'danger',
        container: 'bottom-left',
      })
      return;
    }
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'X-CSRFToken': csrftoken,
      },
      body: JSON.stringify({
        username: usernameRef.current.value,
        password1: passwordRef.current.value,
        password2: passwordConfirmRef.current.value,
      }),
    }
    console.log(requestOptions.body)
    fetch('/rest-auth/registration/', requestOptions)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        if (data.username) {
          console.log(data.username.join(' ,'))
          for (const message of data.username){
            props.Notification({
              message: message,
              title: 'Error',
              type: 'danger',
              container: 'bottom-left',
            })
          }
        } else if (data.password1) {
          for (const message of data.password1){
            props.Notification({
              message: message,
              title: 'Error',
              type: 'danger',
              container: 'bottom-left',
            })
          }
        } else if (data.password2) {
          for (const message of data.password2){
            props.Notification({
              message: message,
              title: 'Error',
              type: 'danger',
              container: 'bottom-left',
            })
          }
        } else {
          history.push('/')
          history.go(0)
        }
      })
      .catch((error) => {
        props.Notification({
          message: error,
          title: 'Error',
          type: 'danger',
          container: 'bottom-left',
        })
      })
    console.log('Finish')
  }
  function onKeyPress(e) {
    if (e.key === 'Enter') {
      onSignUpButton()
    }
  }
  return (
    <div style={{margin: '5px'}}>
      {/* <Button
        variant="outlined"
        onClick={() => {
          settransitState(!transitState)
        }}
        style={{width: '100%', backgroundColor: '#FFC106'}}
      >
        Signup
      </Button> */}
      <CSSTransition in={props.loginAction} timeout={1000} classNames="menu">
        {props.loginAction === true ? (
          <Card style={{backgroundColor: 'white'}}>
            <Card.Body>
              <Form>
                <Form.Group id="email">
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    type="text"
                    ref={usernameRef}
                    required
                    style = {{backgroundColor: '#F2F2F2'}}
                    onKeyDown={(e) => onKeyPress(e)}
                  />
                </Form.Group>
                <Form.Group id="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    ref={passwordRef}
                    style = {{backgroundColor: '#F2F2F2'}}
                    required
                    onKeyDown={(e) => onKeyPress(e)}
                  />
                </Form.Group>
                <Form.Group id="password-confirm">
                  <Form.Label>Password Confirmation</Form.Label>
                  <Form.Control
                    type="password"
                    ref={passwordConfirmRef}
                    required
                    style = {{backgroundColor: '#F2F2F2'}}
                    onKeyDown={(e) => onKeyPress(e)}
                  />
                </Form.Group>
              </Form>

              <Button
                type="submit"
                onClick={onSignUpButton}
                style={{backgroundColor: '#2C2E3A', color: 'white', marginTop: '10px'}}
              >
                {' '}
                Sign Up
              </Button>
            </Card.Body>
          </Card>
        ) : (
          <div></div>
        )}
      </CSSTransition>
    </div>
  )
}
