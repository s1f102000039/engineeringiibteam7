import React from 'react'
import {Grid} from '@material-ui/core'
import {Button, Card} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {getCookie} from '../helper'
import {Icon} from '@iconify/react'
import {UserContext} from './UserContext'
import {Link, useHistory} from 'react-router-dom'

//notification
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'animate.css'
import Notification from './Notification'

export default function UserInfo({user}) {
  const {username, setusername} = useContext(UserContext) // logging in account
  const [avaDir, setAvaDir] = useState('..' + user.avatar) // initially display
  const pathname = window.location.pathname
  const history = useHistory()
  useEffect(() => {
    setAvaDir('..' + user.avatar)
  }, [user])
  const [ava, setAva] = useState() //use while changing avatar

  const imageHandler = (e) => {
    const reader = new FileReader()
    reader.onload = () => {
      if (reader.readyState === 2) {
        setAvaDir(reader.result)
      }
    }
    reader.readAsDataURL(e.target.files[0])
    setAva(e.target.files[0])
    // console.log(e.target.files[0])
  }

  const submitAvaHandler = () => {
    // console.log('ava: ', ava)
    const uploadData = new FormData()
    uploadData.append('username', user.username)
    uploadData.append('Avatar', ava)
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'X-CSRFToken': csrftoken}, // don't add content-type
      body: uploadData,
    }
    fetch('/api/change-avatar/', requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log('yay')
        setAva()
        Notification({message: 'Avatar changed ', container: 'bottom-right'})
      })
      .catch((error) => {
        setAva()
        Notification({
          message: 'Cannot handle the uploaded file',
          container: 'bottom-right',
          title: 'Failed',
          type: 'danger',
          animationIn: ['animate__animated', 'animate__bounceIn'],
        })
      })
  }

  return (
    <div className="user-card">
      <div className="upper-container">
        <div className="image-container">
          <img
            src={avaDir}
            alt={user.username}
            onError={({currentTarget}) => {
              currentTarget.onerror = null // prevents looping
              currentTarget.src = '/static/avatar_default.jpeg'
            }}
            height="100px"
            width="100px"
          />

          {user.username === username && (
            <div>
              <input
                type="file"
                name="image-upload"
                id="img-input"
                accept="image/*"
                onChange={imageHandler}
                style={{display: 'none'}}
              />
              <label htmlFor="img-input" className="change-avatar">
                <Icon icon="clarity:edit-solid" width="20" height="20" />
              </label>
              {ava && (
                <Icon
                  icon="teenyicons:tick-circle-outline"
                  onClick={submitAvaHandler}
                  width="32px"
                  height="32px"
                  className="submit-avatar"
                />
              )}
            </div>
          )}
        </div>
      </div>

      <div className="lower-container">
        <h2>{user.username}</h2>
        <h4>Experience Level: {user.experience_level}</h4>
        <p>{user.description}</p>
        {pathname != '/member/' && (
          <button
            type="button"
            className="btn btn-outline-info"
            style={{
              position: 'absolute',
              bottom: '0%',
              left: '50%',
              transform: 'translate(-50%, -50%)',
            }} // absolute center child
            onClick={() => {
              history.push(`/member?username=${user.username}`)
              history.go(0)
            }}
          >
            Visit profile
          </button>
        )}
      </div>
    </div>
  )
}
