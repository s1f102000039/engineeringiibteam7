import {InputBase, Button} from '@material-ui/core'
import React from 'react'
import {useState, useEffect, useContext} from 'react'
import {UserContext} from './UserContext'
import {getCookie} from '../helper'
import {Grid} from '@material-ui/core'
import {alpha, makeStyles} from '@material-ui/core/styles'
import {CSSTransition} from 'react-transition-group'
import {width} from '@mui/system'
import {useHistory} from 'react-router-dom'

//get current location
import {useLocation} from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  search: {
    position: 'relative',
    marginRight: '4px',
    borderRadius: '30px 30px 30px 30px',
    // borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 1),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.black, 0.15),
    },
    marginLeft: 0,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(2)}px)`,
    transition: theme.transitions.create('width'),
  },
}))
export default function CreateDeck() {
  const {username, setusername} = useContext(UserContext)
  const [deckName, setdeckName] = useState('')
  const classes = useStyles()
  const [transitState, settransitState] = useState(false)
  const history = useHistory()
  const location = useLocation()
  function onCreateDeck() {
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
      body: JSON.stringify({
        deckName: deckName,
      }),
    }
    fetch('/api/create-deck/', requestOptions)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        console.log(data)
        console.log(deckName)
        localStorage.removeItem('curDeck')
        // console.log(location)
        if (location['pathname']!='/word' || !location['search']){
          // console.log("about to go to my profile's page");
          history.push(`/member/?username=${username}`)
          history.go(0)
        }
        else{
          window.location.reload();
        }
        
      })
  }
  return (
    <div>
      {/* <div className="navbar-button">Create New Deck</div> */}
      <div style={{marginTop: '30px'}}>
        <Grid container>
          <Grid item xs={9} className={classes.search}>
            <InputBase
              
              placeholder="Create new deck"
              type="text"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              style={{width: '100%'}}
              onChange={(event) => setdeckName(event.target.value)}
              onKeyPress={(e) => {
                if (e.key === 'Enter') {
                  onCreateDeck()
                }
              }}
            />
          </Grid>
          <Grid>
            <Button
              className="search-button"
              variant="contained"
              style={{
                backgroundColor: '#F7B400',
                width: '60px',
                marginLeft:'7px',
                borderRadius: '30px 30px 30px 30px',
              }}
              onClick={onCreateDeck}
            >
              Create
            </Button>
          </Grid>
        </Grid>

        {/* </CSSTransition> */}
      </div>
    </div>
  )
}
