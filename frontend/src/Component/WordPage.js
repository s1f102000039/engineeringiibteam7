import React , {useState, useEffect, useContext} from 'react'
import {useLocation} from 'react-router-dom'
import {Grid, Button, Typography} from '@material-ui/core'
import NavigationBar from './NavigationBar'
import SearchBar from './SearchBar'
import SuggestedWordBar from './SuggestedWordBar'
import WordDetail from './WordDetail'
import Popup from './Popup'
import Wordkana from './Wordkana'
import LocalFloristOutlinedIcon from '@mui/icons-material/LocalFloristOutlined'
//User
import {UserContext} from './UserContext'
//notification
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'animate.css'
import Notification from './Notification'


export default function WordPage() {
  const search = useLocation().search
  const [word, setword] = useState(new URLSearchParams(search).get('q'))
  const [suggestedWords, setsuggestedWords] = useState([])
  const [buttonpopup, setbuttonpopup] = useState(false)
  const [wordDetail, setwordDetail] = useState({
    text: '',
    gloss: [],
    text_kana: '',
    jlpt_level: "N/A",
  })
  const {username, setusername} = useContext(UserContext)


  useEffect(() => {
    if (word){ // searching word cannot be empty 
      // console.log("Word page re-render")
      fetch(`/search?word=${word}`)
      .then((response) => response.json())
      .then((data) => {
        console.log('data: ', data)
        setwordDetail({
          text: data['ans'][0].text,
          gloss: data['ans'][0].gloss,
          text_kana: data['ans'][0].text_kana,
          jlpt_level: data['ans'][0].jlpt_level,
        })
        setsuggestedWords(
          data['ans'].map((res, index) => (
            <div style={{width: '100%'}} className="related-searches-item">
              <Button
                style={{
                  textAlign: 'left',
                  padding: '10px',
                  width: '100%',
                  display: 'flex',
                  justifyContent: 'left',
                }}
                key={index}
                onClick={() => {
                  setword(res.text)
                  console.log(res.text_kana)
                  setwordDetail({text: res.text, gloss: res.gloss, text_kana: res.text_kana, jlpt_level: res.jlpt_level})
                }}
              >
                <div className="suggested-bar--items">
                  <div className="suggested-bar--items-text">
                    {res.text != res.text_kana ? `${res.text}, ${res.text_kana}` : `${res.text}`}
                  </div>

                  {res.gloss.join(', ')}
                </div>
              </Button>
            </div>
          )),
        )
      })
    }
    
  }, [])

  return (
    <div>
      <NavigationBar></NavigationBar>
      <div className="content">
        <Grid container wrap="nowrap" spacing={1} item xs={12}>
          <Grid item xs={3}>
            {suggestedWords.length != 0 && (
              <SuggestedWordBar suggestedWords={suggestedWords}></SuggestedWordBar>
            )}
          </Grid>
          <Grid item xs={6}>
            
            <div style={{textAlign: 'center'}}>
            {/* <span>
                <img src={'/static/logo.png'} height="70px" width="160px" />
              </span> */}
              <span>
                <h1 className="page-title">The Best Japanese Dictionary Ever</h1>
              </span>
            </div>
           
            <SearchBar></SearchBar>
            <Grid item xs={12}>
              {suggestedWords.length != 0 && <Wordkana wordDetail={wordDetail} word={word} />}
              {suggestedWords.length != 0 && (
                <div className="button-wrapper">
                  <div className="flower-icon">
                    <LocalFloristOutlinedIcon />
                  </div>
                  <div className="jlpt">JLPT LEVEL : 
                  <span> {wordDetail.jlpt_level}</span>
                  </div>
                  <div className="flower-button">
                    <Button
                      className='search-button'
                      style={{backgroundColor: '#fee07f71', borderRadius: '30px', marginLeft: '10px'}}
                      // style={{
                      //   margin: '10px',
                      //   borderRadius: '28px',
                      //   backgroundColor: 'rgba(255,210,93,1)',
                      //   color: 'black',
                      //   boxShadow: 'none',
                      // }}
                      onClick={() => {
                        if (username)
                          setbuttonpopup(true)
                        else {
                          Notification({
                            message: 'You must be logged in first! (Click the button at the top-right corner 😉😉)',
                            container: 'bottom-left',
                            title: 'Failed',
                            type: 'danger',
                            animationIn: ['animate__animated', 'animate__bounceIn'],
                          })
                        }
                      }}
                      disableTouchRipple="true"
                      variant="contained"
                    >
                      Add to deck
                    </Button>
                  </div>
                </div>
              )}

              {suggestedWords.length != 0 && <WordDetail wordDetail={wordDetail} word={word} />}
            </Grid>
          </Grid>
        </Grid>
      </div>
      <Popup wordDetail={wordDetail} trigger={buttonpopup} setbuttonpopup={setbuttonpopup}>
        <h1>Your Decks</h1>
      </Popup>
    </div>
  )
}
