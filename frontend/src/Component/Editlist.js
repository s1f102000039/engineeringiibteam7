import React from 'react'
import EditCase from './EditCase'
import {useRef} from 'react'
import {getCookie} from '../helper'

function Editlist(props) {
  return (
    <div>
      {props.elem.map((item, index) => {
        return <EditCase word={item} key={item.id} id = {item.id} curDeck={props.curDeck} perm={props.perm} handleDelete={props.handleDelete} handleSubmit={props.handleSubmit} edit= {false}/>
      })}
      {props.curDeck && <div className={props.perm? 'coming-soon' : 'coming-soon-none'} onClick={props.createNewWord}>
          <p>Add New Words</p>
      </div>}
    </div>
  )
}

export default Editlist
