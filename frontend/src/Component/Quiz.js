import Zoom from 'react-reveal/Zoom'
import React from 'react'
import {Card} from 'react-bootstrap'

export default function Quiz(props) {
  const quiz_style = {
    backgroundColor: props.quiz_background_color,
  }
  let selections = props.selections.map((item, index) => {
    let isSelected = index === props.curAnswer[props.quiz_index] - 1
    let isDisabled = props.score != -1
    let isRight = index === props.answer - 1

    let backgroundColor = ''
    let fontColor = 'black'

    if (isDisabled) {
      backgroundColor = 'rgba(255, 255, 255, 0.748)'
      if (isRight) backgroundColor = 'green' // right answer
      else if (isSelected) backgroundColor = 'rgb(247, 95, 120)' // wrong answer
      else fontColor = 'rgba(0, 0, 0, 0.349)'
    } else if (isSelected) {
      backgroundColor = 'gray'
    }
    const selection_style = {
      // backgroundColor: props.isSelected ? "#59E391" : "white"
      margin: '0px',
      backgroundColor: backgroundColor,
      color: fontColor,
    }
    // console.log(index, props.curAnswer[props.quiz_index]-1)
    console.log(props.question, index, props.index)
    return (
      <div key={`${props.index}-${index}`} style={{transition: 'right 1s ease-in-out'}}>
        <input
          style={{display: 'none'}}
          type="radio"
          id={`${props.index}-${index}`}
          name={`${props.quiz_index}`}
          value={`${index + 1}`}
          checked={props.curAnswer[props.quiz_index] - 1 === index}
          onChange={props.handleChange}
          disabled={isDisabled}
        />
        <label style={{margin: '10px', width: '100%'}} htmlFor={`${props.index}-${index}`}>
          <div className={`quiz--selection`} style={selection_style} key={index}>
            <div className="card--body">{item}</div>
          </div>
        </label>
      </div>
    )
  })
  return (
    <Zoom duration={400}>
      <div className="quizzes" style={quiz_style}>
        <h2 className="quiz--question">{props.question} </h2>
        <div className="quiz--selections">{selections}</div>
      </div>
    </Zoom>
  )
}
