import React from 'react'
import {Button, Grid} from '@material-ui/core'
import {useHistory} from 'react-router-dom'

export default function LeaderBoardDeck(props) {
  const history = useHistory()
  return (
    <Button style={{width: '100%', textTransform: 'lowercase', borderTop: '1px #808080 solid', height: '70px'}} key={props.deck.deckID}>
      <Grid item xs={2}>
        {props.index + 1}
      </Grid>
      <Grid
        item
        xs={4}
        onClick={() => {
          history.push('/deck/?deckID=' + props.deck.deckID)
        }}
      >
        {props.deck.deckName}
      </Grid>
      <Grid item xs={3}>
        {props.deck.duplicate_time}
      </Grid>
      <Grid
        item
        xs={3}
        onClick={() => {
          history.push('/member?username=' + props.deck.owner)
        }}
      >
        {props.deck.owner}
      </Grid>
    </Button>
  )
}
