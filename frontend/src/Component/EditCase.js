import React, {useState, useRef} from 'react'
import {getCookie} from '../helper'
import {Box, Button, IconButton} from '@material-ui/core'
import Notification from './Notification'
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'animate.css'
import ModeEditRoundedIcon from '@mui/icons-material/ModeEditRounded'
import DeleteIcon from '@mui/icons-material/Delete'
import CheckCircleOutlineTwoToneIcon from '@mui/icons-material/CheckCircleOutlineTwoTone'
import TextareaAutosize from 'react-autosize-textarea';


function EditCase(props) {
  const [Perm, setPerm] = useState(true)
  const textRef = useRef()
  const definitionRef = useRef()
  const [text, setText] = useState(props.word.text)
  const [definition, setDefinition] = useState(props.word.definition)
  const [edit, setedit] = useState(props.word.edit)


  console.log("Display: ", props)


  return (
    <div className="edit-container" >

      <div style={{display: 'flex', justifyContent: 'space-between'}}>
        <IconButton disabled={!props.perm} className='delete-button' onClick={() => props.handleDelete(props.id)}>
          <DeleteIcon />
        </IconButton>
       
        <IconButton disabled={!props.perm} className="edit-button" variant="contained" onClick={() => setedit(!edit)}>
          <ModeEditRoundedIcon />
        </IconButton>
      </div>
      <div className="case-container">
        <div className="edit-form" style={{display: edit ? 'flex' : 'none'}}>
          <form onSubmit= {(event) => props.handleSubmit(event, definitionRef.current.value, textRef.current.value, props.id, props.word.deckID)}>
            <label className="form-text">
              <div >
                <TextareaAutosize ref={textRef} className = "input-text"  type="text" defaultValue={props.word.text} rows={1} maxRows={12}/>
              </div>
            </label>
            <label className="form-definition">
                <TextareaAutosize className = "input-definition" ref={definitionRef} type="text" defaultValue={props.word.definition} rows={1} maxRows={12}/>
            </label>
            <IconButton
              type="submit"
              value="Submit"
              onClick={() => {
                setedit(!edit)
                setText(textRef.current.value)
                setDefinition(definitionRef.current.value)
              }}
            >
              <CheckCircleOutlineTwoToneIcon />
            </IconButton>
          </form>
        </div>
        <div className="edit-case" style={{display: !edit ? 'flex' : 'none'}}>
          <div className="edit-text">{text}</div>
          <hr></hr>
          <p className="edit-definition">{definition}</p>
        </div>
      </div>
    </div>
  )
}

export default EditCase
