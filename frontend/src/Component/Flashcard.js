import React, {useState} from 'react'


export default function Flashcard(props) {
    const [Flip, setFlip] = useState(false)
    return (
        <div className={`flashcard ${Flip? 'flip' : ''}`} onClick={() => setFlip(!Flip)}>
            <div className='front'>
                {props.word.text}
            </div>
            <div className='back'>
                {props.word.definition}
            </div>
        </div>
    )
}
