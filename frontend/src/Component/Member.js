import React, { useCallback } from 'react'
import {useState, useEffect, useContext} from 'react'
import {UserContext} from './UserContext'
import {useLocation, useRouteMatch} from 'react-router-dom'
import {Link, useHistory} from 'react-router-dom'
import {Grid, Typography} from '@material-ui/core'
import NavigationBar from './NavigationBar'
import SearchBar from './SearchBar'
import SuggestedWordBar from './SuggestedWordBar'
import {Button, Card} from 'react-bootstrap'
import {CSSTransition} from 'react-transition-group'
import Ads from './Ads'
import DuplicateDeck from './DuplicateDeck'
import CreateDeck from './CreateDeck'

import Fade from 'react-reveal/Fade'
import Zoom from 'react-reveal/Zoom'
import Flashcard from './Flashcard'
import Carousel from './Carousel'
import Slide from 'react-reveal/Slide'
import UserCard from './UserCard'
import {getCookie} from '../helper'
import Editlist from './Editlist'

//notification
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'animate.css'
import Notification from './Notification'

export default function Member() {
  const [decksList, setDecksList] = useState([]) // save the data for this page

  // get params from url
  const history = useHistory()
  const searchParam = useLocation().search
  const [memberParam, setMemberParam] = useState(new URLSearchParams(searchParam).get('username'))
  const [viewingUser, setViewingUser] = useState('')
  const {username, setusername} = useContext(UserContext)

  const [token, settoken] = useState('')
  const [deckElements, setDeckElements] = useState([])
  const [curDeck, setCurDeck] = useState('')
  const [curDeckElements, setCurDeckElements] = useState([])
  const [deckItem, setdeckItem] = useState([])
  const [deckContent, setDeckContent] = useState('')
  const [isNotified, setIsNotified] = useState(false)
  const [FinishedLoading, setFinishedLoading] = useState(false)
  const [Perm, setPerm] = useState(true)
  const [scroll, setScroll] = useState(true)
  // console.log('local curDeck: ', localStorage.getItem('curDeck'))
  function changeDeck(id) {
    setCurDeck(id)
  }

  function scrollToTop() {
    window.scrollTo({
      top: 0,
      behavior: 'auto',
      /* you can also use 'auto' behaviour
         in place of 'smooth' */
    })
  }

  function handleDelete(elemId) {
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'DELETE',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
      body: JSON.stringify({
        deckID: curDeck,
        id: elemId,
      }),
    }
    fetch('/api/delete-word/', requestOptions)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        console.log(data)
      })
      .then(() =>{
        setCurDeckElements(oldElements => oldElements.filter(elem => elem.id != elemId))
        setdeckItem(oldElements => oldElements.filter(elem => elem.id != elemId))
      })
  }

  const handleSubmit = useCallback((event, definition, text, id, curdeck) => {
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
      body: JSON.stringify({
        deckID: curdeck,
        definition: definition,
        text: text,
        id: id,
      }),
    }
    // If id is available -> EDIT
    if(id){
    fetch('/api/edit-word/', requestOptions)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        console.log(data)
      })
      .then(() => {
        Notification({message: 'Edited'})
      })
      .then(() => {  
        if (memberParam)
          return fetch('/api/get-user-decks' + '?username=' + memberParam)
            .then((response) => {
              if (!response.ok) {
                console.log('No user')
              }
              return response.json()
            })
            .then((data) => {
              try {
                const curDeckData = data.decks.find((element) => element.id === curdeck)
                console.log("Decks list: ", curDeckData)
                setdeckItem(
                  curDeckData.wordsList.map((item, index) => {
                    console.log('Edited item: ', item)
                    return {
                      text: item.text,
                      definition: item.definition,
                      id:item.id
                    }
                  }),
                )
              } catch (error) {
                console.log('Cannot find the data for this user: ', memberParam) // change to notification in the future
              }
            })
      })
   
    }
    
    // If id is NOT available -> CREATE
    else{
      const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
        body: JSON.stringify({
          deckID: curdeck,
          definition: definition,
          text: text,
        }),
      }
      fetch('/api/save-to-deck/', requestOptions)
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          data['Success']
            ? Notification({message:'New word added'})
            : Notification({
                message: 'Failed, word either does not exist or has been deleted',
                type: 'danger',
                title: 'Failed',
              })
        })
    }
    event.preventDefault()
  },[])

  const scrollToBottom = () =>{
    window.scrollTo({
      top: document.body.scrollHeight, 
      behavior: 'smooth',
    })
  }

  useEffect(() => {
    const timer = setTimeout(() => {
      scrollToBottom();
    }, 100);
    return () => clearTimeout(timer);
  }, [scroll]);

  function createNewWord () {
    const newWord = {
        text: '',
        definition: '',
        edit: true,
        create: true,
        deckID: curDeck
    }
      setCurDeckElements(prevElems => [...prevElems, newWord])
      scrollToBottom()
      setScroll(!scroll)
    }
  
  // fetch deck data with username
  useEffect(() => {
    setViewingUser(memberParam)

    // console.log(viewUser)

    if (memberParam != localStorage.getItem('viewingUser')) {
      localStorage.removeItem('curDeck')
      localStorage.setItem('viewingUser', memberParam)
    }

    if (memberParam)
      return fetch('/api/get-user-decks' + '?username=' + memberParam)
        .then((response) => {
          if (!response.ok) {
            console.log('No user')
          }
          return response.json()
        })
        .then((data) => {
          try {
            setDecksList(data.decks)
            console.log('curDeck: ', localStorage.getItem('curDeck'))
            console.log('data: ', data.decks)
            if (data.decks.length) {
              if (!localStorage.getItem('curDeck')) setCurDeck(data.decks[0].id)
              else setCurDeck(localStorage.getItem('curDeck'))
            }

            setViewingUser(data.viewingUser)
            setFinishedLoading(true)
            setPerm(memberParam === username ? true : false)
            // console.log('viewingUser: ', viewingUser.username)
            // console.log('Username: ', username)
            // setPerm(memberParam === username ? true : false)
            // console.log('memberParam: ', memberParam)
          } catch (error) {
            console.log('Cannot find the data for this user: ', memberParam) // change to notification in the future
          }
        })
  }, [memberParam, username])

  useEffect(() => {
    // cannot scroll to top
    // window.scrollTo(0, 0)
    if (window.location.hash == '#duplicatedsuccess' && !isNotified) {
      setIsNotified(true)
      Notification({message: 'Duplicated successfully', container: 'bottom-left'})
      // window.scrollTo(0, 0) c
    }
  }, [])

  // display content
  useEffect(() => {
    // display Decks sidebar
    if (curDeck) {
      localStorage.setItem('curDeck', curDeck)
      const curDeckData = decksList.find((element) => element.id === curDeck)

      // console.log('New curDeck: ', curDeck)

      setDeckElements(
        decksList.map((item) => {
          // console.log('ID:', item.id, 'Cur:', curDeck)
          return (
            <div
              key={item.id}
              className={`deck-sidebar--elements ${item.id == curDeck ? 'current-deck' : ''}`}
              onClick={() => changeDeck(item.id)}
            >
              <h3 className={`${item.id == curDeck ? 'text-selected' : ''} `}>{item.name}</h3>
            </div>
          )
        }),
      )

      //display current Deck content
      if (decksList.length) {
        // console.log('Cur Deck data: ', curDeckData)
        // console.log('wordsList: ', curDeckData.wordsList)
        setdeckItem(
          curDeckData.wordsList.map((item, index) => {
            console.log('Item: ', item)
            return {
              text: item.text,
              definition: item.definition,
              id:item.id
            }
          }),
        )
        setCurDeckElements(
          curDeckData.wordsList.map((item, index) => {
            console.log('Item: ', item)
            return {
              text: item.text,
              definition: item.definition,
              id: item.id,
              deckID: item.deckID,
            }
          }),
        )
      }
    }
  }, [curDeck, handleSubmit])

  

  function handleDuplicate() {
    DuplicateDeck(curDeck, history, username)
  }

  useEffect(() => {
    setDeckContent(
      <Zoom spy={curDeck} duration={300} right>
        <div className="words-display">
          {/* {curDeckElements.length > 0 && (
            <div className="deck-action--button">
              <Button
                className="test--button"
                variant="outline-secondary"
                onClick={handleDuplicate}
              >
                {' '}
                Duplicate this deck{' '}
              </Button>
              <Button
                className="test--button"
                variant="outline-info"
                // href={``}  // duplicate api
                href={`../deck/practice?deckID=${curDeck}`}
              >
                {' '}
                Practice with this deck{' '}
              </Button>
              <Button
                className="test--button"
                variant="outline-danger"
                href={`../deck/game?deck=${curDeck}`} // duplicate api
              >
                {' '}
                Play game{' '}
              </Button>
            </div>
          )} */}
          <h2 className="deck-content--header">Deck content</h2>
          {!curDeckElements.length ?<p style={{fontSize: "20px",textAlign: "center" }}>This deck doesn't have any words yet</p>:''}
          {FinishedLoading && <Editlist elem={curDeckElements} curDeck={curDeck} perm={Perm} handleDelete={handleDelete} handleSubmit={handleSubmit} createNewWord={createNewWord}/>}
        </div>
      </Zoom>,
    )
  }, [curDeckElements, Perm, viewingUser])

  return (
    <div>
      <ReactNotification />
      <NavigationBar></NavigationBar>
      <div className="content">
        <Grid item xs={3} className="deck-sidebar" style={{}}>
          <div className="deck-sidebar--header">
            {viewingUser.username === username ? (
              <div>My decks</div>
            ) : (
              <div>{viewingUser.username}'s decks </div>
            )}
            <hr></hr>
          </div>
          <div className="deck-side">
            <div className="deck-list custom-scrollbar">{deckElements}</div>
            <div>
            <CreateDeck />

            </div>
          </div>
        </Grid>
        <Grid item xs={5} className="deck-content">
          <Carousel word={deckItem} curDeck={curDeck} />
          {curDeckElements.length > 0 && (
            <div className="deck-action--button">
              <Button
                className="test--button"
                variant="outline-secondary"
                onClick={handleDuplicate}
              >
                {' '}
                Duplicate this deck{' '}
              </Button>
              <Button
                className="test--button"
                variant="outline-info"
                // href={``}  // duplicate api
                href={`../deck/practice?deckID=${curDeck}`}
              >
                {' '}
                Practice with this deck{' '}
              </Button>
              <Button
                className="test--button"
                variant="outline-danger"
                href={`../deck/game?deck=${curDeck}`} // duplicate api
              >
                {' '}
                Play game{' '}
              </Button>
            </div>
          )}
        </Grid>
        <Grid item xs={2}>
          <UserCard user={viewingUser} />
        </Grid>
      </div>

      <div className="deck-content-container">
        <div className="deck-content-element">{deckContent}</div>
      </div>
    </div>
  )
}
