import React from 'react'
import {Button, Grid, Typography} from '@material-ui/core'
import {useState} from 'react'
import {useEffect} from 'react'
import GameCard from './GameCard'
import NavigationBar from './NavigationBar'
import {useHistory, useLocation} from 'react-router-dom'
import Confetti from 'react-confetti'
let curCard = -1
let deletedCard = []
let errorCount = 0
let score = 0
let winningState = 0
// let endGameState = 0
let BunnyGif = [
  'https://cdn.dribbble.com/users/1665077/screenshots/7062943/media/7aed8b4de7f70aa975acbbcfceac4ad7.gif', // cheerful
  'https://cdn.dribbble.com/users/1665077/screenshots/7063443/media/844dcd47b32dde6a7d781460e0e94e72.gif', // Sad
  'https://cdn.dribbble.com/users/1665077/screenshots/6940630/crying-bunny.gif', // Crying
  'https://cdn.dribbble.com/users/1665077/screenshots/7064456/media/8b81154d6b25827ef68ed72582dfd35f.gif', // thumbup
]
export default function Game() {
  const [cardList, setcardList] = useState([])
  const [cardElements, setcardElements] = useState()
  const searchParam = useLocation().search
  const [curDeck, setCurDeck] = useState(new URLSearchParams(searchParam).get('deck'))
  const [newGameState, setnewGameState] = useState(0)
  const [bunnyGif, setbunnyGif] = useState(4)
  const history = useHistory()
  const [endGameState, setendGameState] = useState(0)
  // const [errorCount, seterrorCount] = useState(0)
  function handleOnclick(id) {
    if (deletedCard[id] === 1) {
      console.log('Found')
      return
    }
    setcardList((oldCard) =>
      oldCard.map((card) => {
        return card.id === id ? {...card, isClicked: !card.isClicked} : card
      }),
    )
    if (curCard == id) {
      curCard = -1
    } else if (curCard == -1) {
      // If this is the first selected card.
      curCard = id
    } else {
      // If two card is the same.
      if (Math.floor(id / 2) === Math.floor(curCard / 2)) {
        deletedCard[id] = 1
        deletedCard[curCard] = 1
        score += 50
        setcardList((oldCard) =>
          oldCard.map((card) => {
            return Math.floor(card.id / 2) === Math.floor(id / 2)
              ? {...card, isClicked: false, isDeleted: true}
              : card
          }),
        )
      } else {
        // console.log('Bug', curCard, id)
        let temp = curCard
        setcardList((oldCard) =>
          oldCard.map((card) => {
            return card.id === temp || card.id === id ? {...card, isClicked: false} : card
          }),
        )
        errorCount++
      }
      curCard = -1
    }
    console.log(curCard)
  }
  useEffect(() => {
    fetch('/api/get-game-deck/' + '?deck=' + curDeck)
      .then((res) => res.json())
      .then((data) => {
        // For bad request / might need to handle inside the res -> res.json
        if (data['Bad Request']) {
          alert(data['Bad Request'])
          history.push('/')
        }
        setcardList(data['data'])
      })
      .catch((error) => {
        console.log('error', error)
      })
    curCard = -1
    for (let i = 0; i < 20; ++i) deletedCard[i] = 0
    errorCount = 0
    score = 0
    winningState = 0
    setendGameState(0)
  }, [newGameState])
  useEffect(() => {
    setcardElements(
      cardList.map((card) => {
        return (
          <GameCard
            value={card.text}
            key={card.id}
            isClicked={card.isClicked}
            isDeleted={card.isDeleted}
            handleOnclick={() => handleOnclick(card.id)}
          ></GameCard>
        )
      }),
    )
  }, [cardList])
  // Check the end game condition
  useEffect(() => {
    winningState |= score == 500
    setendGameState(endGameState | (score == 500) | (errorCount >= 3))
  }, [score, errorCount])
  useEffect(() => {
    setbunnyGif(errorCount)
  }, [errorCount])
  return (
    <div>
      <NavigationBar></NavigationBar>
      {winningState === 1 && <Confetti  style={{position: 'fixed'}} className="confetti" />}
      {endGameState===0 && (
        <Grid container spacing={3} className="gameCenter">
          <Grid
            item
            xs={2}
            className="gameInfo"
            style={{border: '1px black solid', marginTop: '15%', marginRight: '10px'}}
          >
            <Grid
              style={{justifyContent: 'center', display: 'flex', fontSize: '30px', color: 'black'}}
            >
              SCORE
            </Grid>
            <div className="scoreboard"> {score}</div>
            {/* <Grid
              style={{justifyContent: 'center', display: 'flex', fontSize: '30px', color: 'black'}}
            >
              ERROR
            </Grid>
            <div className="scoreboard"> Error: {errorCount} / 3</div> */}
            <Button
              className="btn-grad"
              variant="outlined"
              style={{width: '100%', color: 'white', marginTop: '10px'}}
              onClick={() => {
                setnewGameState(!newGameState)
              }}
            >
              {endGameState ? 'New Game' : 'Restart Game'}
            </Button>
            <Button
              className="btn-grad"
              variant="outlined"
              style={{marginTop: '10px', width: '100%', color: 'white'}}
              onClick={() => {
                setendGameState(1)
              }}
            >
              End Game
            </Button>
            {bunnyGif != 4 && (
              <img
                src={BunnyGif[bunnyGif]}
                style={{marginTop: '20px', width: '100%', position: 'bottom'}}
              ></img>
            )}
            {/* https://i.imgur.com/DPMGyzx.gif */}
          </Grid>

          <Grid item xs={9} className="gameBoard">
            {cardElements}
          </Grid>
        </Grid>
      )}
      {endGameState != 0 && (
        <Grid className="gameCenter" align-items="center">
          <Grid
            className="gameNotification"
            style={{
              border: '1px black solid',
              padding: '50px',
              marginLeft: '30%',
              marginRight: '30%',
            }}
          >
            <Grid
              style={{justifyContent: 'center', display: 'flex', fontSize: '30px', color: 'black'}}
            >
              SCORE
            </Grid>
            <div className="scoreboard"> {score}</div>
            <Button
              className="btn-grad"
              variant="outlined"
              style={{width: '100%', color: 'white'}}
              onClick={() => {
                setnewGameState(!newGameState)
              }}
            >
              {endGameState ? 'New Game' : 'Restart Game'}
            </Button>
            <img
              style={{width: '100%'}}
              src="https://64.media.tumblr.com/c75cc37ad69374df606688efc48cf1b7/tumblr_nestrw4qVe1r3rdh2o1_500.gifv"
            ></img>
          </Grid>
        </Grid>
      )}
    </div>
  )
}
