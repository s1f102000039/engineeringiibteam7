import React, {Component, useState, useEffect, useContext} from 'react'
import InputBase from '@material-ui/core/InputBase'
import Tooltip  from '@material-ui/core/Tooltip'
import {Container} from '@mui/material'
import {Navbar, Nav, NavDropdown, Offcanvas, Form, FormControl} from 'react-bootstrap'
import {Button, Grid, Typography} from '@material-ui/core'
import Login from '../Auth/Login'
import {getCookie} from '../helper'
import Logout from '../Auth/Logout'
import Signup from '../Auth/Signup'
import CreateDeck from './CreateDeck'
import {UserContext} from './UserContext'
import SearchIcon from '@material-ui/icons/Search'
import {useStyles} from './SearchBar'
import {alpha, makeStyles} from '@material-ui/core/styles'
import {useHistory} from 'react-router-dom'
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'animate.css'
import Grow from '@mui/material/Grow';
import Zoom from 'react-reveal/Zoom'
import Notification from '../Component/Notification'
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown'
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp'
import {typography} from '@mui/system'

export default function NavigationBar() {
  const {username, setusername} = useContext(UserContext)
  const [avatar, setavatar] = useState('')
  const [token, settoken] = useState('')

  const [searchContent, setSearchContent] = useState('')
  const [searchResults_u, setSearchResults_u] = useState([])
  const [searchResults_d, setSearchResults_d] = useState([])
  const [suggestBoxClass, setSuggestBoxClass] = useState('disappear-div') //display suggest box or not
  const [loginAction, setloginAction] = useState(true)
  const classes = useStyles()
  const history = useHistory()
  useEffect(() => {
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
    }

    fetch('/api/is-logged-in/', requestOptions)
      .then((res) => res.json())
      .then((data) => {
        setusername(data.username)
        setavatar(".." + data.curUser.avatar)
        // console.log(username)
      })
  }, [])

  useEffect(() => {
    if (searchContent) {
      fetch(`/api/search-user-deck?q=${searchContent}`)
        .then((response) => response.json())
        .then((data) => {
          // console.log(data)
          setSearchResults_u(
            data['users'].map((res, index) => {
              const avaDir = '..' + res.avatar
              // console.log(avaDir)
              return (
                <div
                  key={index}
                  className="nav-suggest-item-u"
                  onClick={() => {
                    history.push(`/member?username=${res.username}`)
                    history.go(0)
                  }}
                >
                  <Zoom duration={200}>
                    <div className="button-content" style={{}}>
                      <div className="button-content-text">
                        <h2>{res.username}</h2>
                        <p className="overflow-text">{res.description}</p>
                      </div>
                      <img
                        src={avaDir}
                        onError={({currentTarget}) => {
                          currentTarget.onerror = null // prevents looping
                          currentTarget.src = '/static/avatar_default.jpeg'
                        }}
                        className="button-content-img"
                      />
                    </div>
                  </Zoom>
                </div>
              )
            }),
          )
          setSearchResults_d(
            data['decks'].map((res, index) => {
              return (
                <div
                  key={index}
                  className="nav-suggest-item-d"
                  onClick={() => {
                    history.push(`/deck?deckID=${res.deckID}`)
                    history.go(0)
                  }}
                >
                  <Zoom duration={200}>
                    <div className="button-content" style={{}}>
                      <div className="button-content-text-deck">
                        <h2 className="overflow-deckname">{res.deckName}</h2>
                        <p>Contains {res.wordList.length} cards </p>
                      </div>
                    </div>
                  </Zoom>
                </div>
              )
            }),
          )
        })
    }
  }, [searchContent])

  function login(token) {
    settoken(token)
  }
  function logout() {
    settoken('')
  }
  function handleChange(event) {
    // console.log(event.target.value)
    const {name, value, type, checked} = event.target
    setSearchContent(value)
    // console.log(value)
  }

  return (
    <div>
      <ReactNotification />
      <Navbar
        bg="light"
        expand="false"
        fixed="top"
        style={{
          paddingTop: '0px',
          paddingBottom: '0px',
        }}
      >
        <Container
          className="mynavbar"
          style={{
            paddingTop: '10px',
            display: 'inline-flex',
            maxWidth: '100%',
          }}
        >
          <Navbar.Brand href="/">
            <img
              src="/static/logo.png"
              alt="YayDict"
              onError={({currentTarget}) => {
                currentTarget.onerror = null // prevents looping
                currentTarget.src = '/static/logo.png'
              }}
              height="32"
              width="auto"
            />
          </Navbar.Brand>
          <Tooltip
          fontSize = '100px'
          TransitionComponent={Grow}
          leaveDelay = {50}
          title={username ?'' : <p style={{ color: "lightblue", fontSize: "14px", padding: "5px 0px 5px 0px",marginBottom: "0px", textAlign: "center" }}>Create your own profile by logging in with the sidebar!</p>}
          arrow
          >
            <span>
              <Nav.Link
                style={{color: `${username ? 'rgba(0,0,0,.5)' : 'rgba(100,100,100,0.5)'}`, display: 'block'}}
                className={`mynavlink ${username ? '' : 'disabled'}`}
                href={username ? `/member?username=${username}`:'https://www.youtube.com/watch?v=dQw4w9WgXcQ'}
              >
                My profile
              </Nav.Link>
            </span>
          </Tooltip>

          

          <Nav.Link
            style={{color: 'rgba(0,0,0,.5)', display: 'block'}}
            eventKey={2}
            className="mynavlink"
            href="/word"
          >
            Dictionary
          </Nav.Link>

          <Nav.Link
            style={{color: 'rgba(0,0,0,.5)', display: 'block'}}
            eventKey={2}
            className="mynavlink"
            href="/popular"
          >
            Popular
          </Nav.Link>

          <div style={{marginLeft: 'auto', display: 'flex'}} className="navbar--right-side">
            <div className="navbar-search">
              <div className={classes.search} style={{width: '100%'}}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  className="searchInput"
                  value={searchContent}
                  onFocus={(e) => {
                    setSuggestBoxClass('nav-suggest-box')
                  }}
                  onBlur={(e) => {
                    setTimeout(() => {
                      setSuggestBoxClass('disappear-div')
                    }, 300)
                  }}
                  placeholder="Find user/deck..."
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                  }}
                  inputProps={{'aria-label': 'search'}}
                  style={{paddingLeft: '10px'}}
                  onChange={handleChange}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') {
                      history.push(`/word?q=something`)
                      history.go(0)
                    }
                  }}
                />
              </div>
              {/* Display search box only when search bar is focused */}

              <div className={suggestBoxClass} style={{top: '90%'}}>
                {searchResults_u}
                {searchResults_d}
              </div>
            </div>
            <Navbar.Toggle aria-controls="offcanvasNavbar" />
          </div>

          <Navbar.Offcanvas
            style={{backgroundColor: 'cornsilk', transitionTimingFunction: 'cubic-bezier(0.2,0.5,0.3,1)'}}
            id="offcanvasNavbar"
            aria-labelledby="offcanvasNavbarLabel"
            placement="end"
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id="offcanvasNavbarLabel" style={{fontFamily: 'fantasy', fontSize: '30px'}} >JayDict</Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Nav className="justify-content-end flex-grow-1 pe-3">
                {username != '' ? (
                  <div>
                    <div
                      className="navbar-button"
                      onClick={() => {
                        history.push('/word')
                        history.go(0)
                      }}
                    >
                      Dictionary
                    </div>
                    <div
                      className="navbar-button"
                      onClick={() => {
                        history.push('/popular/')
                        history.go(0)
                      }}
                    >
                      Ranking
                    </div>
                    <CreateDeck />

                    <div className="navbarUser">
                      <img
                        src={
                          avatar
                        }
                        alt={username}
                        onError={({currentTarget}) => {
                          currentTarget.onerror = null // prevents looping
                          currentTarget.src = '/static/avatar_default.jpeg'
                        }}
                        height="50px"
                        width="50px"
                        style={{borderRadius: '50%', margin: '5px'}}
                        onClick={() => {
                          history.push('/member?username=' + username)
                          history.go(0)
                        }}
                      />
                      <div
                        style={{width: 'fit-content', display: 'inline-block', margin: '5px'}}
                        onClick={() => {
                          history.push('/member?username=' + username)
                          history.go(0)
                        }}
                      >
                        <div className="navbarUser-username">{username}</div>
                        <div style={{cursor: 'pointer'}}>View profile</div>
                      </div>
                      {/* <Grid>
                        {username}
                      </Grid> */}
                      <Logout onLogout={(token) => logout(token)}></Logout>
                    </div>
                  </div>
                ) : (
                  <div>
                    <div style={{padding: '2px'}}>
                      <Button
                        variant="outlined"
                        onClick={() => setloginAction(!loginAction)}
                        style={{
                          width: '40%',
                          backgroundColor: 'inherit',
                          margin: '5px',
                          color: loginAction !== false ? 'white' : 'black',
                          backgroundColor: loginAction !== false ? '#2C2E3A' : 'inherit',
                        }}
                      >
                        Login{' '}
                        {loginAction === false ? (
                          <ArrowDropUpIcon fontSize="small" />
                        ) : (
                          <ArrowDropDownIcon fontSize="small" />
                        )}
                      </Button>
                      <Login
                        onLogin={(token) => login(token)}
                        loginAction={loginAction}
                        Notification={Notification}
                      ></Login>
                    </div>

                    <div style={{padding: '2px'}}>
                      <Button
                        variant="outlined"
                        onClick={() => setloginAction(!loginAction)}
                        style={{
                          width: '40%',
                          backgroundColor: 'inherit',
                          margin: '5px',
                          color: loginAction === false ? 'white' : 'black',
                          backgroundColor: loginAction === false ? '#2C2E3A' : 'inherit',
                        }}
                      >
                        Signup{' '}
                        {loginAction !== false ? (
                          <ArrowDropUpIcon fontSize="small" />
                        ) : (
                          <ArrowDropDownIcon fontSize="small" />
                        )}
                      </Button>
                      <Signup loginAction={!loginAction} Notification={Notification}></Signup>
                    </div>
                  </div>
                )}
              </Nav>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
        </Container>
      </Navbar>
    </div>
  )
}
