import React from 'react'
import {Grid, Button, IconButton} from '@material-ui/core'
import {useState, useEffect} from 'react'
import {getCookie} from '../helper'
import WordDetail from './WordDetail'
import CloseIcon from '@material-ui/icons/Close'
import Box from '@mui/material/Box'
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import 'animate.css'
import Notification from './Notification'
import Slide from 'react-reveal/Slide'
import CreateDeck from './CreateDeck'

let count = 0

export default function Popup(props) {
  const [islogin, setislogin] = useState(false)
  const [username, setusername] = useState('')
  const [token, settoken] = useState('')
  const [deckElements, setDeckElements] = useState([])
  const [curDeck, setCurDeck] = useState('')
  const [curDeckName, setCurDeckName] = useState('')

  function changeDeck(id, name) {
    setCurDeck(id)
    setCurDeckName(name)
    count++
  }

  useEffect(() => {
    if (count === 0) return
    const csrftoken = getCookie('csrftoken')
    console.log('Fetched')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
      body: JSON.stringify({
        deckID: curDeck,
        definition: props.wordDetail['gloss'].join(', '),
        text: props.wordDetail['text'],
      }),
    }
    fetch('/api/save-to-deck/', requestOptions)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        data['Success']
          ? Notification({message: props.wordDetail['text'] +  ' is added to deck ' + curDeckName})
          : Notification({
              message: 'Failed ' + curDeckName + ' either does not exist or has been deleted',
              type: 'danger',
              title: 'Failed',
            })
      })
    setCurDeck('')
  }, [count])

  useEffect(() => {
    const csrftoken = getCookie('csrftoken')
    const requestOptions = {
      method: 'POST',
      headers: {'Content-Type': 'application/json', 'X-CSRFToken': csrftoken},
    }

    fetch('/api/is-logged-in/', requestOptions)
      .then((res) => res.json())
      .then((data) => {
        setislogin(data.is_logged_in)
        setusername(data.username)
      })
  }, [])

  useEffect(() => {
    if (username)
      return fetch('/api/get-user-decks' + '?username=' + username)
        .then((response) => {
          if (!response.ok) {
            console.log('No user')
          }
          return response.json()
        })
        .then((data) => {
          let decksList = data.decks
          setDeckElements(
            decksList.map((item) => {
              return (
                <Button
                  variant="outlined"
                  style={{backgroundColor: ' rgba(252,242,208,0.7)'}}
                  key={item.id}
                  name={item.name}
                  onClick={() => changeDeck(item.id, item.name)}
                >
                  {item.name}
                </Button>
              )
            }),
          )
        })
  }, [username])

  return props.trigger ? (
    <div className="popup">
      <ReactNotification />
      <Slide top duration={800}>
        <Box display="flex" alignItems="center" className="popup-inner ">
          <Box className="popup--title">
            {props.children}
            <IconButton
              style={{position: 'absolute', right: '15px'}}
              onClick={() => props.setbuttonpopup(false)}
            >
              <CloseIcon />
            </IconButton>
          </Box>

          <Box className="popup--deck custom-scrollbar ">{deckElements}</Box>
          <div id="deck-create">
            <CreateDeck />
          </div>
        </Box>
      </Slide>
    </div>
  ) : (
    ''
  )
}
