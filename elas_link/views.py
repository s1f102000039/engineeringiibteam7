from django.shortcuts import render

# Create your views here.
def index(request, *args, **kwargs):
    packet= {
        'header' : 'JayDict | View Elastic search infomation',
    }
    return render(request, 'frontend/index.html', packet)