## Run Django server
```
python manage.py makemigrations && python3 manage.py migrate
python manage.py runserver
```

## Install virtual env, install packages:
    Create conda env => activate the env
  ```
    pip install -r requirements.txt 
  ```
## Run frontend: 
```
    npm run dev
```
## Build frontend:
```
    npm run build
```
## Deploy to heroku: 
```
    git push heroku master / git push heroku main
```

### how to fix unable to login/signup:
    - delete db.sqlite3
    - delete api/Migration
    - python manage.py makemigrations api
    - python manage.py migrate
    - Run server


# Beautify
Using prettier to beautify code.

## Note
- Primary key is VERY IMPORTANT while using many-to-many

# Create logo, favicon from PNG:
https://www.remove.bg/upload

# Deploy to heroku
https://dev.to/mdrhmn/deploying-react-django-app-using-heroku-2gfa

# Heroku config
```
DATABASE_URL:          postgres://***
NODE_MODULES_CACHE:    false
NPM_CONFIG_PRODUCTION: false
USE_NPM_INSTALL:       true
```

