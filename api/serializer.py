from django.db import models
from rest_framework import serializers
from .models import Deck, User, Word

#Deck related Serializer
class DeckSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deck
        fields = (
            'wordList', 'deckName', 'owner', 'deckID', 'duplicate_time'
        )
class CreateDeckSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deck
        fields = (
            'deckName',
        )

class UserSerializer(serializers.ModelSerializer) :
    class Meta: 
        model = User
        fields = ('username', 'deckList', 'description', 'avatar', 'experience_level', 'is_admin', 'is_staff', 'is_superuser','duplicate_time')
    
class UserDecksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deck
        fields = ('username', 'deckList')


class WordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Word
        fields = (
            'definition', 'text', 'deckID', 'added_at', 'id',
        )

class AddToDeck(serializers.ModelSerializer): 
    class Meta:
        model = Word
        fields = (
            'definition', 'text', 'deckID',
        )

class DuplicateDeckSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deck
        fields = (
           'deckName', 'owner', 'deckID'
           )
